/**
 * 
 */
package Person;
import java.util.ArrayList;

import Facility.Facility;
import Facility.IFacility;
import Services.IFacilityUse;

/*
 * Person.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */

/** Note: We could have made a separate class that implemented IUsage interface, but that really doesn't makes much 
 * difference because then we would have to create objects of that class in the main class to access only two functions
 * defined by the IUsage interface.
 * 
 */

public class Person {

	private String id = null;
	private String fname = null;
	private String middlename = null;
	private String lname = null;
	private String DOB = null;
	private ArrayList<Phone> phone = null;
	private ArrayList<Address>address = null;
	private String email = null;
	public ArrayList<IFacility> usesFacility = new ArrayList<IFacility> (); //change facility to IfacilityUse
	
	public Person(){
		this.address = new ArrayList<Address>();
		this.phone = new ArrayList<Phone>();
		//usesFacility = new ArrayList<IFacility>();
	}
	
	public Person(String id, String Fname, String Lname, String dob){
		this.id = id;
		this.fname = Fname;
		this.lname = Lname;
		this.DOB = dob;
	}
	
	public String getId(){
		return this.id;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getFirstName(){
		return this.fname;
	}
	
	public void setFirstName(String Fname){
		this.fname = Fname;
	}
	
	public String getLastName(){
		return this.lname;
	}
	
	public void setLastName(String Lname){
		this.lname = Lname;
	}
	
	public String getMiddleName(){
		return this.middlename;
	}
	
	public void setMiddleName(String mname){
		this.middlename = mname;
	}
	
	public String getFullName(){
		if (this.middlename != null)
			return this.fname + " " + this.middlename + " " + this.lname;
		else 
			return this.fname + " " + this.lname;
	}
	
	public ArrayList<Address> getAddress(){
		return this.address;
	}
	
	public void addAddress(Address address){
		this.address.add(address);
	}
	
	public boolean removeAddress(Address address){
		if (this.address.contains(address)){
			this.address.remove(address);
		}
		return !this.address.contains(address);
	}
	
	public ArrayList<Phone> getPhone(){
		return this.phone;
	}
	
	public void addPhone(Phone phone){
		this.phone.add(phone);
	}
	
	public boolean removePhone(Phone phone){
		if (this.phone.contains(phone)){
			this.phone.remove(phone);
		}
		return !this.phone.contains(phone);
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public String getDOB(){
		return this.DOB;
	}
	
	public void setDOB(String dob){
		this.DOB = dob;
	}
	
	public void listPersonFacilities(){
		for (IFacility f : usesFacility)
			System.out.println(f.getFacilityInfo());
	}

}//end class
	
