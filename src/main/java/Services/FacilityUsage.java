/**
 * 
 */
package Services;

import Facility.Facility;
import Facility.IFacility;
import Facility.Inspection;
import Person.Person;

/*
 * FacilityUsage.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */

public class FacilityUsage implements IFacilityUse{

	public FacilityUsage(){}
	
	
	public boolean isInUseDuringInterval() {
		// TODO Auto-generated method stub
		return false;
	}

	public void assignFacilityToUse(Person person, IFacility facility) {
		person.usesFacility.add(facility);
		
	}

	public boolean vacateFacility(Person person, IFacility facility) {
		if (person.usesFacility.contains(facility))
			person.usesFacility.remove(facility);
		return true;
	}

	public void addInspection(IFacility facility, Inspection inspection) {
		facility.addInspection(inspection);
	}
	
	public void listInspection(IFacility facility) {
		for(Inspection i : facility.getInspections()){
			System.out.println(i.toString());
		}
	}
	
	public void listActualUsage(IFacility facility) {
		System.out.println(facility.getUsage());		
	}

	public double calcUsageRate() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	public boolean isInUseDuringInterval(Time interval) {
		//check between two time intervals and if yes then return true
		return false;
	}**/

}
