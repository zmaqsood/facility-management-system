/**
 * 
 */
package Services;

import Facility.Facility;
import Facility.IFacility;
import Facility.Inspection;
import Person.Person;
/*
 * IFacilityUse.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */
public interface IFacilityUse {
	
	public boolean isInUseDuringInterval();
	public void assignFacilityToUse(Person person, IFacility facility);
	public boolean vacateFacility(Person person, IFacility facility);
	public void listInspection(IFacility facility);
	public void listActualUsage(IFacility facility);
	public double calcUsageRate();
	public void addInspection(IFacility facility, Inspection inspection);

}
