/**
 * 
 */
package Services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import Facility.Facility;
import Facility.IFacility;
import Facility_Maintainance.Maintainance;

/*
 * MaintainanceRequest.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */
public class MainRequest implements IMaintainanceUsage{

	private Facility facility;
	private Maintainance maintainance;
	private ArrayList<IFacility> facilities = new ArrayList<IFacility>(); 
	public HashMap<Maintainance,IFacility> maintainanceRequests = new HashMap<Maintainance,IFacility>(); //request for this maintainance came from this facility
	public HashMap<IFacility, ArrayList<Maintainance>> scheduledMaintainances = new HashMap<IFacility, ArrayList<Maintainance>>(); //this facility needs this maintainance
	private ArrayList <IFacility> temp = new ArrayList<IFacility>();

	
	public MainRequest(){}
	
	/**
	public MainRequest(Facility fac, Maintainance maintain){
		facility = fac;
		maintainance = maintain;		
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public Maintainance getMaintainance() {
		return maintainance;
	}

	public void setMaintainance(Maintainance maintainance) {
		this.maintainance = maintainance;
	}**/
	
	
	//interfaces methods begin here
	public void makeFacilityMaintRequest(Maintainance maintainance, IFacility facility) {
		maintainanceRequests.put(maintainance, facility);
	}
	
	public void listMaintainanceRequest() {
		for (Entry<Maintainance, IFacility> entry : maintainanceRequests.entrySet())
			System.out.println("Maintainance type: " + entry.getKey().getType() + " made by facility" + entry.getValue().getFacilityInfo());
	}

	public void scheduleMaintainance(IFacility facility, ArrayList<Maintainance> maintainance) {
		scheduledMaintainances.put(facility,maintainance);
	}
	
	public void listMaintainance() {
		for (Entry<IFacility, ArrayList<Maintainance>> entry : scheduledMaintainances.entrySet()){
			System.out.println("Facility: " + entry.getKey().getFacilityInfo() + " requested following maintainances");
			for (Maintainance m : entry.getValue()){
				System.out.print(m.getType() + ", ");
			}
		}
	}

	public void calcMaintainanceCost(IFacility facility) {
		double cost = 0.0;		
		for (Entry<IFacility, ArrayList<Maintainance>> entry : scheduledMaintainances.entrySet()) {
            if (entry.getKey() == facility) {
            	for (Maintainance m : entry.getValue()){
                cost += m.getCost();}
            }
        }
		System.out.println(cost);
	}

	//some hardcoded values here to check for problem rate
	public void calcProblemRate(IFacility facility) {
		int problemrate = 0;
		for (Entry<Maintainance, IFacility> entry : maintainanceRequests.entrySet()){
			if (entry.getValue() == facility){
				if (entry.getKey().getType() == "water")
					problemrate = 1;
				else if (entry.getKey().getType() == "gas")
					problemrate = 2;
				else if (entry.getKey().getType() == "electric")
					problemrate = 5;
			}
		}
		System.out.println(problemrate);
	}

	public void calcDownTime(IFacility facility) {
		// TODO Auto-generated method stub
	}

	//check to see if the scheduledMaintainances contains an entry with the specified facility? if so then list it's 
	//related maintainance
	public void listFacilityProblems(IFacility facility) {
		for (Entry<IFacility, ArrayList<Maintainance>> entry : scheduledMaintainances.entrySet()){
			if (entry.getKey() == facility){
				for (Maintainance m : entry.getValue())
					System.out.println(m.getType());
			}
		}
	}
	
	
}//end class

