/**
 * 
 */
package Services;

import java.util.ArrayList;

import Facility.IFacility;
import Facility_Maintainance.Maintainance;
/*
 * MaintainanceUsage.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */
public interface IMaintainanceUsage {

	public void makeFacilityMaintRequest(Maintainance maintainance, IFacility facility);
	public void scheduleMaintainance(IFacility facility, ArrayList<Maintainance> maintainance);
	public void calcMaintainanceCost(IFacility facility);
	public void calcProblemRate(IFacility facility);
	public void calcDownTime(IFacility facility);
	public void listMaintainanceRequest();
	public void listMaintainance();
	public void listFacilityProblems(IFacility facility);//you can make it to list problems for all the facilities a list maintained by implementing class
}
