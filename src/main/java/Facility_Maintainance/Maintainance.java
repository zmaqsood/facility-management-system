package Facility_Maintainance;
import java.util.ArrayList;
import Facility.IFacility;


/*
 * Maintainance.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */

public class Maintainance {
	private String Id;
	private String Type;
	private double Cost;
	private ArrayList<IFacility> facilities = new ArrayList<IFacility> ();
	
	public Maintainance(){
		this.Id = null;
		this.Type = null;
		this.Cost = 0.0;
	}
	
	public Maintainance(String id, String type, double cost){
		this.Id = id;
		this.Type = type;
		this.Cost = cost;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}
	
	public void setCost(double cost){
		Cost = cost;
	}
	
	public double getCost(){
		return Cost;
	}
	
	//remove this method if you don't need hashmap <Maintainance, Facility> in the MainRequest class;
	/**
	public boolean addFacility(IFacility facility){
		facilities.add(facility);
		return true;
	}**/ 
	
}
