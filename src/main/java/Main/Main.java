package Main;

import java.util.ArrayList;

import Facility.Facility;
import Facility.Floors;
import Facility.IFacility;
import Facility.Inspection;
import Facility.Unit;
import Facility_Maintainance.Maintainance;
import Person.Address;
import Person.Person;
import Services.FacilityUsage;
import Services.IFacilityUse;
import Services.IMaintainanceUsage;
import Services.MainRequest;

/*
 * Main.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */

//corresponds to the main class(s) for creating and calling different objects.
public class Main 
{
    public static void main( String[] args )
    {
        Address add1 = new Address ("1", "101 West Wabash", "", "Chicago","IL","60611","USA");
        Address add2 = new Address ("2", "300 West Wabash", "", "Chicago","IL","60611","USA");
        Address add3 = new Address ("3", "400 West Wabash", "", "Chicago","IL","60611","USA");
        Address add4 = new Address ("4", "161 East Chicago Ave", "", "Chicago","IL","60611","USA");
        Address add5 = new Address ("5", "747 North Wabash Ave", "", "Chicago","IL","60610","USA");
        Address add6 = new Address ("6", "25 East Superior St", "", "Chicago","IL","60611","USA");
        Address add7 = new Address ("7", "1032 W. Sheridan Rd", "850 N Michigan Ave", "Chicago","IL","60660","USA");
        


        Facility F1 = new Facility("1", "Residential", "100 * 100", 10, add1);
        Facility F2 = new Facility("2", "Commercial", "100 * 100", 10, add2);
        Facility F3 = new Facility("3", "Office", "100 * 100", 10, add3);
        Facility F4 = new Facility("4", "Multi_Use", "600 * 17540", 292, add4);
        Facility F5 = new Facility("5", "Residental", "350 * 1803", 171, add5);
        Facility F6 = new Facility("6", "Residental", "500 * 700", 225, add6);
        Facility F7 = new Facility("7", "Educational", "1600 * 2900", 47, add7);
        
        
        F1.setUsage(1.1);
        F2.setUsage(142.1);
        F4.setUsage(524190.90);
        F6.setUsage(1313.12);
        F7.setUsage(12413412.93);
        
        
        Unit u1 = new Unit ("1", 10.0, 10.0, 10.0, "Office");
        Unit u2 = new Unit ("2", 10.0, 10.0, 10.0, "apartment");
        Unit u3 = new Unit ("3", 10.0, 10.0, 10.0, "shop");
        
        Floors f1 = new Floors("1", 150, 1, 120.31, 241.41, 100, 4);
        Floors f2 = new Floors("80", 50, 20, 120.41, 241.41, 100, 4 );
        
        //F4.addNewUnit(u1);
        //F4.addNewUnit(u2);
        //F4.addNewUnit(u3);
        
        
        Inspection I1 = new Inspection ("1", "12:00 pm", "General inspection");
        Inspection I2 = new Inspection ("2", "1:00 pm", "Electric inspection");
        Inspection I3 = new Inspection ("3", "12:00 pm", "Gas inspection");
        
        Person p1 = new Person ("1", "Zain", "Maqsood", "12-12-70");
        Person p2 = new Person ("2", "Irfan", "Raziuddin", "12-01-88");
        Person p3 = new Person ("3", "Zain", "Maqsood", "12-03-99");
        Person p4 = new Person ("4", "Neiman", "Marcus", "");
        
        Maintainance m1 = new Maintainance("1", "water", 100.50);
        Maintainance m2 = new Maintainance("2", "gas", 10.50);
        Maintainance m3 = new Maintainance("3", "electric", 150.50);
        
        IFacilityUse FacUse1 = new FacilityUsage ();
        FacUse1.assignFacilityToUse(p1,F1);
        FacUse1.assignFacilityToUse(p1,F2);
        FacUse1.vacateFacility(p1, F2);
        FacUse1.addInspection(F1, I1);
        System.out.println("Inspections for Facility: " + F1.getFacilityInfo());
        FacUse1.listInspection(F1);
        System.out.println();
        System.out.println("Facilities used by person: " + p1.getFullName());
        p1.listPersonFacilities();
        System.out.println();
        System.out.println("Actaul usage for facility: " + F1.getFacilityInfo());
        FacUse1.listActualUsage(F1);
        System.out.println();
        
        //MainRequest
        IMaintainanceUsage MainUsage = new MainRequest();
        MainUsage.makeFacilityMaintRequest(m1, F1);
        MainUsage.makeFacilityMaintRequest(m2, F2);
        MainUsage.makeFacilityMaintRequest(m3, F1);
        System.out.println("Maintainance requests made by facilitites are: ");
        System.out.println();
        MainUsage.listMaintainanceRequest();
        System.out.println();
        System.out.println("Scheduled maintainances are: ");
        System.out.println();
        ArrayList<Maintainance> temp = new ArrayList<Maintainance>();
        temp.add(m1);
        temp.add(m3);
        MainUsage.scheduleMaintainance(F1, temp);
        MainUsage.listMaintainance();
        System.out.println();
        System.out.println();
        System.out.println("Maintainance cost for facility F1");
        MainUsage.calcMaintainanceCost(F1);
        System.out.println();
        System.out.println("Problem rate for maintainance is: ");
        MainUsage.calcProblemRate(F1);
        
        System.out.println();
        
        
    }
}
