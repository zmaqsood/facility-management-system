/**
 * 
 */
package Facility;

import java.util.ArrayList;

/*
 * IFacility.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */
public interface IFacility {

	public void listUnits();
	public String getFacilityInfo();
	public int getAvailableCapacity();
	public boolean addNewUnit(Unit unit);
	public void addFacilityDetail(String detail);
	public boolean removeUnit(Unit unit);
	public ArrayList<Inspection> getInspections();
	public void addInspection(Inspection inspection);
	public double getUsage();
	
}
