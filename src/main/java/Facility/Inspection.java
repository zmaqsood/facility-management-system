/**
 * 
 */
package Facility;
/*
 * Inspection.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */
public class Inspection {
	
	private String id;
	private String time;
	private String summary;
	
	public Inspection() {
		id = null;
		time = null;
		summary = null;
	}

	public Inspection(String id, String time, String summary) {
		this.id = id;
		this.time = time;
		this.summary = summary;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	public String toString() {
		return summary + " " + this.time;
	}

}//end class
