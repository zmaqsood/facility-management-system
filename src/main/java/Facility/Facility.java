/**
 * 
 */
package Facility;

import java.util.ArrayList;

import Facility_Maintainance.Maintainance;
import Person.Address;
import Person.Person;

/*
 * Facility.java
 *
 * L O Y O L A  U N I V E R S I T Y  C H I C A G O
 * 
 * COMP-473 - Advanced Object Oriented Programming - Spring 2014
 *
 * Irfan Raziuddin <iraziuddin@luc.edu>
 * Zain Maqsood <zmaqsood@luc.edu>
 *
 */


/**
 * Note: 
 * 1. We maintain a list of facilities for each facility that holds all the facilities that it can
 * have. We didn't implement any specific facility (e.g. house, apartment, shop because they are all subtype of facility
 * and any subtype that's added in future would just have to implement the facility interface and "this" class 
 * can hold it's objects (list of facility), so makes design more extensible and flexible.
 * 2. Floors are made a separate entity because a floor can have any number of facility and to be able to access any
 * facility on any floor we've to make it separate entity, you can't make floor as an attribute of facility because if
 * you do so then you won't be able to achieve this functionality which makes the design more fragile.
 * 
 */

public class Facility implements IFacility{
	
	private String id = null;
	
	//facility type: any type of facility, could have made it an entity but all of the subclasses won't be doing much 
	//so made a general entity
	private String facilityType = null; 
	private String dimensions = null;
	private Address address;
	private int capacity;
	private String detail = null;
	private ArrayList<Floors> numFloors;
	private ArrayList<Unit> Units;
	public ArrayList<Inspection> inspections = new ArrayList<Inspection>(); 
	public ArrayList<Maintainance> maintainances = new ArrayList<Maintainance>();
	private double usage;
	
	public Facility(){
		//if no arguements passed set them as default
		this.dimensions = "1000 * 1000";
		this.address = null;
		this.capacity = 5;
		this.facilityType = "general";
		numFloors = new ArrayList<Floors>(10);
  	    Units = new ArrayList<Unit>(10);
	}
	
	public Facility(String id, String type, String Dim, int Capacity, Address address){
		this.setId(id);
		this.facilityType = type;
		this.dimensions = Dim;
		this.address = address;
		this.capacity = Capacity;
		inspections = new ArrayList<Inspection>();
	}
	
	public void listUnits() {
		for (Unit u : Units){
			System.out.println(u.getId());
		}
	}

	public String getFacilityInfo() {
		return this.getId() + " " + this.facilityType + " " + this.address;
	}
	

	public int getAvailableCapacity() {
		return capacity;
	}

	public boolean addNewUnit(Unit unit) {
		if (!this.Units.contains(unit.getId())){
			this.Units.add(unit);
			return true;
			}
		else return false;
	}

	public void addFacilityDetail(String s) {
		this.detail = s;
	}

	public boolean removeUnit(Unit unit) {
		if (!this.Units.contains(unit)){
			this.Units.remove(unit);
			return true;
			}
		else return false;
	}
	
	public void setUsage(double use){
		usage += use;
	}

	public double getUsage(){
		return usage;
	}
	
	public ArrayList<Inspection> getInspections(){
		return inspections;
	}

	public void addInspection(Inspection inspection) {
		inspections.add(inspection);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the facilityType
	 */
	public String getFacilityType() {
		return facilityType;
	}

	/**
	 * @param facilityType the facilityType to set
	 */
	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	/**
	 * @return the dimensions
	 */
	public String getDimensions() {
		return dimensions;
	}

	/**
	 * @param dimensions the dimensions to set
	 */
	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}
	

	/**
	public void addInspection(Inspection inspection){
		this.inspections.add(inspection);
	}
	
	public void listInspection() {
		for (Inspection i : this.inspections){
			System.out.println(i.toString());
		}
	}

	public boolean isInUseDuringInterval() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void listActualUsage() {
		// TODO Auto-generated method stub
		
	}

	public double calcUsageRate() {
		// TODO Auto-generated method stub
		return 0;
	}**/

	
}//end class
